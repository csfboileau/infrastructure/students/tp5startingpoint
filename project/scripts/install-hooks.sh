#!/usr/bin/env bash

GIT_DIR=$(git rev-parse --git-dir)
echo "Running from: " $PWD
echo "Git dir is: " $GIT_DIR

echo "Installing hooks..."
ln ../../project/scripts/pre-commit.sh $GIT_DIR/hooks/pre-commit
echo "Done"
