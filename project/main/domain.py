from flask import *
from main.customExceptions import *
from main import db
from main import version

app = Flask('DB user management program')

def hello():
	return 'Welcome back to super mega hot user program, V2.' +  version.getVersion()

def getUsers():
    return {"InitialUsers": db.getInitialUsersFromDB(), "NewUsers": db.getNewUsersFromDB()}

def delUser(username):
	if username in db.getInitialUsersFromDB():
		raise InitialUserException(username + " is an initial user, cannot delete.")
	elif not username in db.getNewUsersFromDB():
		raise NonExistingUserException(username + " does not exist, cannot delete.")
	db.delUserFromDB(username)
	return username + " was deleted."

def addUser(username):
	if username in db.getInitialUsersFromDB():
		raise UserAlreadyExistsException(username + " already exists as initial user, cannot add it.")
	if username in db.getNewUsersFromDB():
		raise UserAlreadyExistsException(username + " already exists as new user, cannot add it.")
	db.addUserToDB(username)
	return username + " was added."