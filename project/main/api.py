from flask import *
from main.customExceptions import *
from main import db
from main import version
from main import domain as dom
import sys

app = Flask('DB user management program')

@app.route('/')
def welcome(): #pragma: no cover
	return dom.hello()

@app.route('/users')
def route_getUsers(): #pragma: no cover
	return make_response(jsonify({'msg': dom.getUsers()}))

@app.route('/users/<username>', methods=['DELETE'])
def route_delUser(username): #pragma: no cover
	try:
		rep = dom.delUser(username)
		return make_response(jsonify({'msg':rep}))
	except InitialUserException as iue:
		return make_response(jsonify({'msg':str(iue)}), 422)
	except NonExistingUserException as neue:
		return make_response(jsonify({'msg':str(neue)}), 404)

@app.route('/users', methods=['POST'])
def route_addUser(): #pragma: no cover
	if 'username' in request.args:
		username = request.args['username']
		try:
			rep = dom.addUser(username)
			return jsonify({'msg':rep})
		except UserAlreadyExistsException as uaee:
			return make_response(jsonify({'msg':str(uaee)}), 422)
	else:
		return make_response(jsonify({'msg':'param username is mandatory for user creation.'}), 400)

if __name__ == '__main__': #pragma: no cover
	if len(sys.argv) >= 2:
		try:
			app_port = int(sys.argv[1])
		except ValueError as ve:
			app_port = 5559

		app.run(debug=True, host='0.0.0.0', port=app_port)
	else:
		raise ValueError('No starting port for the application')

