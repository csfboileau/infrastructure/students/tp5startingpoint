import unittest
import requests
import json

IP = "127.0.0.1"
PORT = "15555"
URL = "http://" + IP + ":" + PORT

class APIDeployTest(unittest.TestCase):

	def test_home(self):
		response = requests.get(URL + "/")
		self.assertEqual(200, response.status_code)

	def test_getUsers(self):
		response = requests.get(URL + "/users")
		self.assertEqual(200, response.status_code)
		map = json.loads(response.content.decode('utf-8'))
		map = map['msg']
		
		expected =  {"InitialUsers": ["Bob", "Body"], "NewUsers": ["Bill", "Jack", "Joe"]}
		self.assertCountEqual(expected, map)
		self.assertCountEqual(expected['InitialUsers'], map['InitialUsers'])

	def test_delUserFail1(self):
		response = requests.delete(URL + "/users/Bob")
		self.assertEqual(422, response.status_code)
		
	def test_delUserFail2(self):
		response = requests.delete(URL + "/users/Al")
		self.assertEqual(404, response.status_code)
		
	def test_delUser(self):
		response = requests.post(URL + "/users?username=James")
		self.assertEqual(200, response.status_code)

		response = requests.delete(URL + "/users/James")
		self.assertEqual(200, response.status_code)
		
		response = requests.get(URL + "/users")
		self.assertEqual(200, response.status_code)
		map = json.loads(response.content.decode('utf-8'))
		map = map['msg']
		
		self.assertNotIn('James', map['NewUsers'])
	
	def test_addUserFail1(self):
		response = requests.post(URL + "/users?username=Bob")
		self.assertEqual(422, response.status_code)

	def test_addUserFail2(self):
		response = requests.post(URL + "/users?username=Bill")
		self.assertEqual(422, response.status_code)
		
	def test_addUser(self):
		response = requests.post(URL + "/users?username=Tom")
		self.assertEqual(200, response.status_code)	
		
		response = requests.get(URL + "/users")
		self.assertEqual(200, response.status_code)
		map = json.loads(response.content.decode('utf-8'))
		map = map['msg']
		self.assertIn('Tom', map['NewUsers'])
		
if __name__ == "__main__":
	unittest.main()
