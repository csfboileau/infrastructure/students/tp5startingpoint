import unittest
import requests
import json

#IP = "tp5-infra.ddnsgeek.com"
IP = "127.0.0.1"
PORT = "15555"
URL = "http://" + IP + ":" + PORT

class APIDeployTest(unittest.TestCase):

	def test_home(self):
		response = requests.get(URL + "/")
		self.assertEqual(200, response.status_code)

	def test_getUsers(self):
		response = requests.get(URL + "/users")
		self.assertEqual(200, response.status_code)
		map = json.loads(response.content.decode('utf-8'))
		map = map['msg']
		
		self.assertIn("InitialUsers", map)
		self.assertIn("NewUsers", map)
		self.assertIn("Body", map["InitialUsers"])

if __name__ == "__main__":
	unittest.main()