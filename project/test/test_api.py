import unittest
import unittest.mock

from main import domain as dom
from main.customExceptions import *

class APIUnitTests(unittest.TestCase):

	def test_hello(self):
		self.assertIn("Welcome", dom.hello())

	@unittest.mock.patch('main.domain.db')
	def test_getUsers(self, mock_db):
		mock_db.getInitialUsersFromDB.return_value = ['Bob', 'Body']
		mock_db.getNewUsersFromDB.return_value = ['Bill', 'Joe', 'Jack']
		map = dom.getUsers()
		expected =  {"InitialUsers": ["Bob", "Body"], "NewUsers": ["Bill", "Jack", "Joe"]}
		self.assertCountEqual(expected, map)
		self.assertCountEqual(expected['InitialUsers'], map['InitialUsers'])
		self.assertCountEqual(expected['NewUsers'], map['NewUsers'])


	@unittest.mock.patch('main.domain.db')
	def test_delUsersFail(self, mock_db):
		mock_db.getInitialUsersFromDB.return_value = ['Bob', 'Body']
		mock_db.getNewUsersFromDB.return_value = ['Bill', 'Joe', 'Jack']
		with self.assertRaises(InitialUserException) as context:
			dom.delUser('Bob') 
		with self.assertRaises(NonExistingUserException) as context:
			dom.delUser('Al') 

	@unittest.mock.patch('main.domain.db')
	def test_delUsers(self, mock_db):
		mock_db.getInitialUsersFromDB.return_value = ['Bob', 'Body']
		mock_db.getNewUsersFromDB.return_value = ['Bill', 'Joe', 'Jack']
		dom.delUser('Bill')
			
	@unittest.mock.patch('main.domain.db')
	def test_addUsersFail(self, mock_db):
		mock_db.getInitialUsersFromDB.return_value = ['Bob', 'Body']
		mock_db.getNewUsersFromDB.return_value = ['Bill', 'Joe', 'Jack']
		with self.assertRaises(UserAlreadyExistsException) as context:
			dom.addUser('Bob') 
		with self.assertRaises(UserAlreadyExistsException) as context:
			dom.addUser('Bill') 

	@unittest.mock.patch('main.domain.db')
	def test_delUsers(self, mock_db):
		mock_db.getInitialUsersFromDB.return_value = ['Bob', 'Body']
		mock_db.getNewUsersFromDB.return_value = ['Bill', 'Joe', 'Jack']
		dom.addUser('Tom')
			
if __name__ == "__main__":
	unittest.main()

